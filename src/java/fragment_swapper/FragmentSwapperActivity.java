/**
"Fragment swapper: 
Create an activity whose layout holds a floating action button, and a container for fragment. 
Define two fragments that will be used by that activity to fill the container.
When the button is clicked, the activity should swap the fragments.
"
*/