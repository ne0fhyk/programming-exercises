package lca;
/**
 * Find lowest common ancestor of two nodes in a binary tree
 * <p>
 * A
 * / \
 * B   C
 * /   /  \
 * D  E    F
 * \
 * G
 * <p>
 * LCA(E, F) => C
 * LCA(G, E) => C
 * LCA(D, G) => A
 * <p>
 * LCA(F, G) => F
 * <p>
 * <p>
 * Test cases
 * 1 - If n1 is the root of the tree, then LCA(n1, n2) => n1
 * <p>
 * G => F -> C -> A
 * <p>
 * D => B -> A
 */

import java.util.HashSet;

/**
 Let's m be the number of parents for node 1
 Let's n be the number of parents for node 2
 */


class Node {
    public final Node left, right, parent;

    Node(Node left, Node right, Node parent) {
        this.left = left;
        this.right = right;
        this.parent = parent;
    }
}

class NodeUtils {
    /**
     * Solution 1: O(m + n)
     */
    public Node findLCA(Node n1, Node n2) {
        if (n1.parent == null) {
            return n1;
        }

        if (n2.parent == null) {
            return n2;
        }

        HashSet<Node> parents = new HashSet<Node>();

        Node parent = n1.parent;
        while (parent != null) {
            parents.add(parent);
            parent = parent.parent;
        }

        //Check if any of the parents for n2 is in the queue.
        parent = n2.parent;
        while (!parents.contains(parent)) {
            parent = parent.parent;
        }

        return parent;
    }

    /**
     * Solution 2: O(max(m, n))
     */
    public Node findLCAOptimized(Node n1, Node n2) {
        if (n1.parent == null) {
            return n1;
        }

        if (n2.parent == null) {
            return n2;
        }

        HashSet<Node> parents = new HashSet<Node>();

        Node parent1 = n1.parent;
        Node parent2 = n2.parent;
        while (parent1 != null || parent2 != null) {
            //Check if parent1 collides with one entry in the hash
            if (parent1 != null) {
                if (parents.contains(parent1)) {
                    return parent1;
                }
                else{
                    parents.add(parent1);
                }

                parent1 = parent1.parent;
            }

            if (parent2 != null) {
                if (parents.contains(parent2)) {
                    return parent2;
                } else {
                    parents.add(parent2);
                }

                parent2 = parent2.parent;
            }
        }

        return null;
    }
}
