/**
"LIFO thread race: 
start 3 concurrent tasks, all calling computeUniverse(). The 3 tasks should finish, in the reverse order from which they started.
computeUniverse() can run for an arbitrary period of time.
"
*/

public class LifoThreadRacer {

	public static void computeUniverse(){
		Thread.sleep(new Random().nextLong());
	}

	public static void main(String... args){
		int racesCount = 5;

		for(int i = 0; i < racesCount; i++){
			runRaces(i);
		}
	}

	private static void runRaces(int index){
		CountDownLatch thirdLatch = new CountDownLatch(0);
		CountDownLatch secondLatch = new CountDownLatch(1);
		CountDownLatch firstLatch = new CountDownLatch(2);
		CountDownLatch mainLatch = new CountDownLatch(3);

		new Thread(new ThreadRacer("First thread", firstLatch, new CountDownLatch[]{mainLatch})).start();
		new Thread(new ThreadRacer("Second thread", secondLatch, new CountDownLatch[]{firstLatch, mainLatch})).start();
		new Thread(new ThreadRacer("Third thread", thirdLatch, new CountDownLatch[]{firstLatch, secondLatch, mainLatch})).start();

		mainLatch.await();
		System.out.println("Completed race #" + index);
	}

	public static class ThreadRacer implements Runnable {

		private final String label;
		private final CountDownLatch latch;
		private final CountDownLatch[] releases;

		public ThreadRacer(String label, CountDownLatch latch, CountDownLatch[] releases){
			this.label = label;
			this.latch = latch;
			this.releases = releases;
		}

		@Override
		public void run(){
			computeUniverse();
			if(latch != null){
				latch.await();
			}

			for(CountDownLatch release: releases){
				release.countDown();
			}

			System.out.println("Thread " + label + " completed");
		}
	}

}