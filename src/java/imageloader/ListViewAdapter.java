/**
"Image Loader: 

class ImageData {
        String imageUrl;
        String imageTitle;
}

1. Given the above data structure, implement the portion of the adapter (ListView or RecyclerView) that populates the view for an instance of that data structure.
2. (optional based on previous implementation) How would the implementation differ if you have a very large amount of images that could be displayed.
3. (if not provided) Provide an in-memory cache implementation.
4. (if not provided) Provide disk cache implementation for the proposed solution.
"
*/

public class ListViewAdapter extends ArrayAdapter<ImageData> {

	private final LruCache<String, Bitmap> bitmapCache = new LruCache(100);

	public ListViewAdapter(Context context, List<ImageData> data){
		super(context, 0, data);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		ImageData data = getItem(position);
		String imageUrl = data.imageUrl;

		if(convertView == null){
			convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_item_layout, parent, false);
		}

		TextView imageTitle = convertView.findViewById(R.id.image_title);
		imageTitle.setText(data.imageTitle);

		ImageView imageView = convertView.findViewById(R.id.image_data);
		//Check if the image was cached already
		Bitmap imageBmp = bitmapCache.get(imageUrl);
		if(imageBmp != null){
			imageView.setImageBitmap(imageBmp);

			ImageLoader previousLoader = (ImageLoader) convertView.getTag();
			if(previousLoader != null){
				previousLoader.clearReferenceView();
				previousLoader.cancel(true);
			}
		}
		else{
			//Load the image asynchronously.
			ImageLoader previousLoader = (ImageLoader) convertView.getTag();
			if(previousLoader != null && !previousLoader.getImageUrl.equals(imageUrl)){
				previousLoader.clearReferenceView();
				previousLoader.cancel(true);

				ImageLoader loadImage = new ImageLoader(imageUrl, imageView);
				loadImage.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

				convertView.setTag(loadImage);
			}
		}
		
		return convertView;
	}

	public static class ImageLoader extends AsyncTask<Void, Void, Bitmap> {

		private final WeakReference<ImageView> imageRef;
		private final String imageUrl;
		private final Map<String, Bitmap> memCache;

		public ImageLoader(String imageUrl, ImageView referenceView, Map<String, Bitmap> memCache){
			this.imageUrl = imageUrl;
			imageRef = new WeakReference<ImageView>(referenceView);
			this.memCache = memCache;
		}

		public String getImageUrl(){
			return imageUrl;
		}

		public void clearReferenceView(){
			this.imageRef.set(null);
		}

		@Override
		public Bitmap doInBackground(){
			
			try{
				String targetUrl = this.imageUrl;

				URL url = new URL(targetUrl);
				Bitmap result = BitmapFactory.decodeStream(url.openConnection().getInputStream());

				return result;
			}catch(Exception e){
				return null;
			}
		}

		@Override
		public void onPostExecute(Bitmap result){
			if(result == null)
				return;

			//Cache the downloaded bitmap
			bitmapCache.put(this.imageUrl, result);	
			if(isCancelled())
				return;

			ImageView referenceView = imageRef.get();
			if(referenceView == null)
				return;

			referenceView.setImageBitmap(result);
		}
	} 
}