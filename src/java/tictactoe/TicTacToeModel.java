package tictactoe;

import java.awt.Point;
import java.util.HashMap;

public class TicTacToeModel implements OnGameMoveListener {

    public interface GameUpdateListener {
        void onInvalidMove(Point moveLocation, Player player);

        void onDraw();

        void onWon(Player player);
    }

    private final HashMap<Point, Player> gameMoves = new HashMap();

    private GameUpdateListener listener;

    public void setGameUpdateListener(GameUpdateListener listener){
        this.listener = listener;
    }

    @Override
    public void onGameMove(Point moveLocation, Player player){
        //Check if the move is valid
        if(gameMoves.contains(moveLocation)){
            if(listener != null){
                listener.onInvalidMove(moveLocation, player);
            }
            return;
        }

        gameMoves.put(moveLocation, player);

        int row = moveLocation.x;
        int col = moveLocation.y;

        //Check if the current player win with 3 in the row
        if(player.equals(gameMoves.get(new Point(row, 0)))
            && player.equals(gameMoves.get(new Point(row, 1)))
            && player.equals(gameMoves.get(new Point(row, 2)))
            ){
            if(listener != null){
                listener.onWon(player);
            }
            return;
        }

        //Check if the current player win with 3 in the col
        if(player.equals(gameMoves.get(new Point(0, col)))
            && player.equals(gameMoves.get(new Point(1, col)))
            && player.equals(gameMoves.get(new Point(2, col)))){
            if(listener != null){
                listener.onWon(player);
            }
            return;
        }

        //Check if the current player win with 3 in diagonal
        if(row == col
            && player.equals(gameMoves.get(new Point(0, 0)))
            && player.equals(gameMoves.get(new Point(1, 1)))
            && player.equals(gameMoves.get(new Point(2, 2)))){
            if(listener != null){
                listener.onWon(player);
            }
            return;
        }

        //Check if the current player win with 3 in the opposite diagonal
        if(row + col == 2
            &&){
            if(listener != null){
                listener.onWon(player);
            }
            return;
        }

        //Check if this is a draw
        if(gameMoves.size() >= 9){
            if(listener != null){
                listener.onDraw();
            }
        }
    }

}