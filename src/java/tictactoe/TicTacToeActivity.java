package tictactoe;

import static javafx.scene.input.KeyCode.R;

/**
 * Created by fredia on 4/3/16.
 */
public class TicTacToeActivity extends AppCompatActivity implements TicTacToeModel.GameUpdateListener {
    private TicTacToeModel model;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_tic_tac_toe);

        model = new TicTacToeModel();
        model.setGameUpdateListener(this);

        TicTacToeView gameView = (TicTacToeView) findViewById(R.id.game_view);
        gameView.setOnGameMoveListener(new OnGameMoveListener(){
            @Override
            public void onGameMove(Point moveLocation, Player player){

            }
        });
    }
}
